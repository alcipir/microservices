var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PrinterSchema = new Schema({
	model: String,
	price: Number
});

module.exports = mongoose.model('printers', PrinterSchema);