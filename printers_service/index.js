const express = require('express')
, path = require('path')
, logger = require('morgan')
, mongoose = require('mongoose')
, config = require('./config')
, Printer = require('./models/printer')
, bodyParser = require('body-parser')
, app = express()
;

const env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

// Middleware
if (env === 'development') app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// connect to MongoDB

mongoose.connect(config.db[app.settings.env], function(err, res) {
  if(err) {
    console.log('MongoDB env: ' + config.db["development"]);
    console.log('Connection failed. ' + err);
  } else {
    console.log('Connected to Database: ' + config.db[app.settings.env]);
  }
});


// Routes

// Get all printers
app.get('/printers', function(req, res, next) {
  Printer.find(function(err, printers) {
    if(err) {
      res.json({ 'ERROR': err});
    } else {
      res.json(printers);
    }
  });
});

// Get a single printer with id {:id}
app.get('/printer/:id', function(req, res, next) {
  Printer.findById(req.params.id, function(err, printer) {

    if(err) {
      res.json({ 'ERROR': err});
    } else {
      res.json(printer);
    }
  });


});

// Get all printers of model {:model}
app.get('/printer/model/:model', function(req, res, next) {
  Printer.find({ model: req.params.model }, function(err, printers) {
    if(err) {
      res.json({ 'ERROR': err});
    } else {
      res.json(printers);
    }
  });
});

// Add printer
app.post('/printers', function(req, res, next) {
  var p = new Printer({ model: req.body.model, price: req.body.price });
  p.save(function(err) {
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json({ 'CREATED': p});
    }
  });

});

// Update an printer
app.put('/printer/:id', function(req, res, next) {

  Printer.findById(req.params.id, function(err, printer) {
    printer.model = req.body.model;
    printer.price = req.body.price;
    printer.save(function(err) {
      if(err) {
        res.json({'ERROR': err});
      } else {
        res.json({'UPDATED': printer});
      }
    });
  });

});

// Delete an printer
app.delete('/printer/:id', function(req, res, next) {

  Printer.findById(req.params.id, function(err, printer) {
    printer.remove(function(err) {
      if(err) {
        res.json({'ERROR': err});
      } else {
        res.json({'DELETED': printer});
      }
    });
  });

});

// 404 catcher
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err,
    title: 'error'
  });
});

// Spin it up!
app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {
  if (env !== 'test') console.log('Printers service listening on port ' + server.address().port);
});

module.exports = server;
