'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'test';

const should = require('chai').should()
, request = require('supertest')

  // start the server for integration tests
  , app = require('../')
  , mongoose = require('mongoose')
  , Printer = require('../models/printer')
  ;


// test the API
describe('Integration Tests', () => {

  Printer.collection.drop();

  beforeEach(function(done) {
    var testPrinter = new Printer({ model: "Replicator Mini", price: 899.00 });
    testPrinter.save(function(err) { done(); });
  });

  it('"GET /printers" should return all printers', done => {
    request(app)
    .get('/printers')
    .expect(200)
    .expect(res => {
      res.should.be.json;
      res.body.should.be.a('array');
      res.body[0].should.have.property('model');
      res.body[0].should.have.property('price');
    })
    .end(done);
  });

  it('"GET /printer/:id" should return printer of id: {:id} ', done => {
    var testPrinter = new Printer({ model: "Replicator", price: 2899.00 });
    testPrinter.save(function(err, data) {
      request(app)
      .get('/printer/' +  data.id)
      .expect(200)
      .expect(res => {
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('_id');
        res.body.should.have.property('model');
        res.body.should.have.property('price');
        res.body._id.should.equal(data.id);
      })
      .end(done);

    });
    
  });

  it('"GET /printer/model/:model" should return printers of model {:model}', done => {
    var testPrinter = new Printer({ model: 'Replicator' , price: 2899.00 });
    testPrinter.save(function(err, data) {
      request(app)
      .get('/printer/model/' +  data.model)
      .expect(200)
      .expect(res => {
        res.should.be.json;
        res.body.should.be.a('array');
        res.body[0].should.have.property('model');
        res.body[0].should.have.property('price');
        res.body[0].model.should.equal(data.model);
      })
      .end(done);

    });

  });

  it('"POST /printers" should add a new Printer', done => {
    var testPrinter = new Printer({ model: "Replicator" , price: 2899.00 });
    request(app)
    .post('/printers')
    .send(testPrinter)
    .expect(200)
    .expect(res => {
      res.body.should.be.a('object');
      res.body.should.have.property('CREATED');
      res.body.CREATED.should.have.property('_id');
      res.body.CREATED.should.have.property('model');
      res.body.CREATED.should.have.property('price');
      res.body.CREATED.model.should.equal(testPrinter.model);
      res.body.CREATED.price.should.equal(testPrinter.price);
    })
    .end(done);
  });

  it('"PUT /printer/:id" should update an printer of id {:id}', done => {
   var testPrinter = new Printer({ model: "Replicator" , price: 2899.00 });
   testPrinter.save(function(err, data) {
     request(app)
     .put('/printer/' + data.id)
     .send({ price: 2599.00, model: data.model })
     .expect(200)
     .expect(res => {
      res.body.should.be.a('object');
      res.body.UPDATED.should.have.property('model');
      res.body.UPDATED.should.have.property('price');
      res.body.UPDATED.price.should.equal(2599.00);
    })
     .end(done);      
   });
 });

  it('"DELETE /printer/:id" should delete an printer of id {:id}', done => {
   var testPrinter = new Printer({ model: "Replicator" , price: 2899.00 });
   testPrinter.save(function(err, data) {
     request(app)
     .delete('/printer/' + data.id)
     .expect(200)
     .expect(res => {
      res.body.should.be.a('object');
      res.body.should.have.property('DELETED');
      res.body.DELETED.should.have.property('model');
      res.body.DELETED.should.have.property('price');
      res.body.DELETED.model.should.equal(testPrinter.model);
      res.body.DELETED.price.should.equal(testPrinter.price);
    })
     .end(done);      
   });
 });


after(() => {
    // shut down the server
    Printer.collection.drop();
    app.close();
  })
});
