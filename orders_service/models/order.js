var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var orderSchema = new Schema({
	customer: String,
	_printersIds: [Schema.Types.ObjectId],
	updated: { type: Date, default: Date.now },
	total: Number,
});

module.exports = mongoose.model('orders', orderSchema);