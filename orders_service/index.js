const express = require('express')
, path = require('path')
, logger = require('morgan')
, mongoose = require('mongoose')
, config = require('./config')
, order = require('./models/order')
, bodyParser = require('body-parser')
, app = express()
;

const env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

// Middleware
if (env === 'development') app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// MongoDB

mongoose.connect(config.db[app.settings.env], function(err, res) {
  if(err) {
    console.log('Connection failed. ' + err);
  } else {
    console.log('Connected to Database: ' + config.db[app.settings.env]);
  }
});


// Routes

// Get all orders
app.get('/orders', function(req, res, next) {
  order.find(function(err, orders) {
    if(err) {
      console.log(err);
      res.json({ 'ERROR': err});
    } else {
      res.json(orders);
    }
  });
});

// Get a single order with id {:id}
app.get('/order/:id', function(req, res, next) {

  order.findById(req.params.id, function(err, order) {
    if(err) {
      res.json({ 'ERROR': err});
    } else {
      res.json(order);
    }
  });


});

// Get all orders for _printerId {:id}
app.get('/order/printer/:id', function(req, res, next) {
  order.find( { _printersIds: { $in: [req.params.id] }  }, function(err, orders) {
    if(err) {
      console.log(err);
      res.json({ 'ERROR': err});
    } else {
      res.json(orders);
    }
  });
});

// Add order
app.post('/orders', function(req, res, next) {
  var p = new order({ _printersIds: req.body._printersIds, customer: req.body.customer, quantity: req.body.quantity, total: req.body.total });
  p.save(function(err) {
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json({ 'CREATED': p});
    }
  });

});

// Update an order
app.put('/order/:id', function(req, res, next) {

  order.findById(req.params.id, function(err, order) {
    order._printersIds = req.body._printersIds;
    order.customer = req.body.customer;
    order.total = req.body.total;
    order.save(function(err) {
      if(err) {
        res.json({'ERROR': err});
      } else {
        res.json({'UPDATED': order});
      }
    });
  }); 

});

// Delete an order
app.delete('/order/:id', function(req, res, next) {

  order.findById(req.params.id, function(err, order) {
    order.remove(function(err) {
      if(err) {
        res.json({'ERROR': err});
      } else {
        res.json({'DELETED': order});
      }
    });
  }); 

});

// 404 catcher
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err,
    title: 'error'
  });
});

// Spin it up!
app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {
  if (env !== 'test') console.log('Order service listening on port ' + server.address().port);
});

module.exports = server;
