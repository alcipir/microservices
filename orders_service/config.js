var config = {};

config.db = {
	development: "mongodb://" + process.env.MONGODB_PORT_27017_TCP_ADDR,
	test: "mongodb://" + process.env.MONGODB_PORT_27017_TCP_ADDR + "/test"
}

module.exports = config;
