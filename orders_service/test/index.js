'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'test';

const should = require('chai').should()
, request = require('supertest')

  // start the server for integration tests
  , app = require('../')
  , mongoose = require('mongoose')
  , Order = require('../models/order')
  ;


// test the API
describe('Integration Tests', () => {

  Order.collection.drop();

  beforeEach(function(done) {
    var test_printer_id = mongoose.Types.ObjectId();
    var testOrder = new Order({ _printersIds: [ mongoose.Types.ObjectId(), test_printer_id ], customer: "Jon Snow", quantity: 1, total: 300.00 });
    testOrder.save(function(err) { done(); });
  });

  it('"GET /orders" should return all orders', done => {
    request(app)
    .get('/orders')
    .expect(200)
    .expect(res => {
      res.should.be.json;
      res.body.should.be.a('array');
      res.body[0].should.have.property('_id');
      res.body[0].should.have.property('customer');
      res.body[0].should.have.property('_printersIds');
      res.body[0].should.have.property('total');
    })
    .end(done);
  });

  it('"GET /order/:id" should return order of id: {:id} ', done => {
    var testOrder = new Order({ _printersIds: [ mongoose.Types.ObjectId(), mongoose.Types.ObjectId() ], customer: "Jon Snow", quantity: 1, total: 300.00 });
    testOrder.save(function(err, data) {
      request(app)
      .get('/order/' +  data.id)
      .expect(200)
      .expect(res => {
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('_id');
        res.body.should.have.property('customer');
        res.body.should.have.property('_printersIds');
        res.body._id.should.equal(data.id);
      })
      .end(done);

    });
    
  });

  it('"GET /order/printer/:id" should return orders for _printerId {:id}', done => {
   var test_printer_id = mongoose.Types.ObjectId();
   var testOrder = new Order({ _printersIds: [ test_printer_id ] , customer: "Jon Snow", total: 3000.00 });
   testOrder.save(function(err, data) {
    request(app)
    .get('/order/printer/' +  test_printer_id)
    .expect(200)
    .expect(res => {
      res.should.be.json;
      res.body.should.be.a('array');
      res.body[0].should.have.property('_id');
      res.body[0].should.have.property('customer');
      res.body[0].should.have.property('_printersIds');
      res.body[0].should.have.property('total');
      res.body[0]._printersIds[0].should.equal( String(test_printer_id) );
      res.body[0].total.should.equal(data.total);
      res.body[0].customer.should.equal(data.customer);
    })
    .end(done);

  });

 });

  it('"POST /orders" should add a new order', done => {
    var testPrinterId_One = mongoose.Types.ObjectId();
    var testPrinterId_Two = mongoose.Types.ObjectId();
    var testOrder = { _printersIds: [ testPrinterId_One, testPrinterId_Two ], customer: "Jon Snow", total: 300.00 };
    request(app)
    .post('/orders')
    .send(testOrder)
    .expect(200)
    .expect(res => {
      res.body.should.be.a('object');
      res.body.should.have.property('CREATED');
      res.body.CREATED.should.have.property('_id');
      res.body.CREATED.should.have.property('_printersIds');
      res.body.CREATED.should.have.property('customer');
      res.body.CREATED.should.have.property('total');
      res.body.CREATED._printersIds[0].should.equal( String(testOrder._printersIds[0]) );
      res.body.CREATED._printersIds[1].should.equal( String(testOrder._printersIds[1]) );
      res.body.CREATED.customer.should.equal(testOrder.customer);
      res.body.CREATED.total.should.equal(testOrder.total);
    })
    .end(done);
  });

  it('"PUT /order/:id" should update an order of id {:id}', done => {
   var testPrinterId_One = mongoose.Types.ObjectId();
   var testPrinterId_Two = mongoose.Types.ObjectId();
   var testOrder = new Order({ _printersIds: [ testPrinterId_One, testPrinterId_Two ], customer: "Jon Snow", total: 300.00 });
   testOrder.save(function(err, data) {
    var new_printers_ids = [ mongoose.Types.ObjectId(), mongoose.Types.ObjectId() ];
    request(app)
    .put('/order/' + data.id)
    .send({ '_printersIds': new_printers_ids, 'total': 1500.00, 'customer': "Sansa Stark" })
    .expect(200)
    .expect(res => {
      res.body.should.be.a('object');
      res.body.should.have.property('UPDATED');
      res.body.UPDATED.should.have.property('_printersIds');
      res.body.UPDATED.should.have.property('customer');
      res.body.UPDATED.should.have.property('total');
      res.body.UPDATED._printersIds[0].should.equal(String(new_printers_ids[0]));
      res.body.UPDATED._printersIds[1].should.equal(String(new_printers_ids[1]));
      res.body.UPDATED.customer.should.equal("Sansa Stark");

      res.body.UPDATED.total.should.equal(1500.00);
    })
    .end(done);      
  });
 });

  it('"DELETE /order/:id" should delete an order of id {:id}', done => {
    var testPrinterId_One = mongoose.Types.ObjectId();
    var testPrinterId_Two = mongoose.Types.ObjectId();
    var testOrder = new Order({ _printersIds: [ testPrinterId_One, testPrinterId_Two ], customer: "Jon Snow",  total: 300.00 });
    testOrder.save(function(err, data) {
     request(app)
     .delete('/order/' + data.id)
     .expect(200)
     .expect(res => {
      res.body.should.be.a('object');
      res.body.should.have.property('DELETED');
      res.body.DELETED.should.have.property('_printersIds');
      res.body.DELETED.should.have.property('customer');
      res.body.DELETED.should.have.property('total');
      res.body.DELETED._printersIds[0].should.equal( String(testOrder._printersIds[0]) );
      res.body.DELETED._printersIds[1].should.equal( String(testOrder._printersIds[1]) );
      res.body.DELETED.customer.should.equal(testOrder.customer);
      res.body.DELETED.total.should.equal(testOrder.total);
    })
     .end(done);      
   });
  });


  after(() => {
    // shut down the server
    Order.collection.drop();
    app.close();
  })
});
