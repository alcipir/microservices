This is a microservices exercise, for educational purposes only.
=========

```````
$ docker build -t orders orders_service/
$ docker build -t printers printers_service/
$ docker-composer up -d
```````

### It will build and run:

* Printers Service
* Orders Service
    * Both services based on [makerbot/node-homework](https://github.com/makerbot/node-homework)
* MongoDB

### You can then use a REST client to do CRUD operations with both services:

![REST example](microservices.gif)
